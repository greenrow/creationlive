<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Likes extends \yii\db\ActiveRecord {
    
        

           public function find_user_likes($proj_id,$user_id){
           $rez=Yii::$app->db->createCommand('SELECT id FROM Likes WHERE user_id=:user_id AND proj_id=:proj_id')->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->query()->count();

        if ($rez==0){


              Yii::$app->db->createCommand("INSERT INTO Likes(proj_id,user_id) VALUES(:proj_id,:user_id)")->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->execute(); 
        }
           if ($rez>0){


              Yii::$app->db->createCommand("DELETE  FROM Likes WHERE proj_id=:proj_id AND user_id=:user_id ")->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->execute(); 
        }

   }
   
   
   public function get_likes($proj_id){
       
       $rez= Yii::$app->db->createCommand("SELECT id FROM likes WHERE proj_id=:proj_id")->bindValues([':proj_id'=>$proj_id])->query()->count();
       return $rez;
       
   }
   

}
