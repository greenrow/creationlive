<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Category extends \yii\db\ActiveRecord {
    
        

function find_categ($name){
    
    
                $count=Yii::$app->db->createCommand('Select id From Category WHERE name=:name')->bindValues([':name'=>$name])->query();
                return count($count);
    
}

               public function setcateg($name){

                Yii::$app->db->createCommand("INSERT INTO Category(name) VALUES(:name)")->bindValues([':name'=>$name])->execute(); 

        }
        
                 public function getcateg_id($name){

               $id_categ= Yii::$app->db->createCommand('Select id From Category Where name=:name')->bindValues([':name'=>$name])->query(); 
             return $id_categ;

        }
        
                 public function getcateg($proj_id){
                $rez=Yii::$app->db->createCommand("Select cat.name FROM Category cat,Categ_Projects catproj  WHERE catproj.categ_id=cat.id AND catproj.proj_id=:proj_id")->bindValues([':proj_id'=>$proj_id])->queryAll(); 
                return $rez;
        }
}
