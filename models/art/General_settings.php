<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class General_settings{
    
        
public function not_show_age($user_id,$par){
    
               
                $rez = Yii::$app->db->createCommand('UPDATE general_settings SET usinfo_age = :par WHERE info_user_id=:user_id')->bindValues([':user_id'=>$user_id,':par'=>$par])->execute(); 
           return $rez;
    
}

public function not_show_email($user_id,$par){
    
               
                $rez = Yii::$app->db->createCommand('UPDATE general_settings SET usinfo_email = :par WHERE info_user_id=:user_id')->bindValues([':user_id'=>$user_id,':par'=>$par])->execute(); 
           return $rez;
    
}

public function not_show_web($user_id,$par){
    
               
                $rez = Yii::$app->db->createCommand('UPDATE general_settings SET usinfo_website = :par WHERE info_user_id=:user_id')->bindValues([':user_id'=>$user_id,':par'=>$par])->execute(); 
           return $rez;
    
}

public function not_show_interes($user_id,$par){
    
               
                $rez = Yii::$app->db->createCommand('UPDATE general_settings SET usinfo_aboutme = :par WHERE info_user_id=:user_id')->bindValues([':user_id'=>$user_id,':par'=>$par])->execute(); 
           return $rez;
    
}

public function not_show_job($user_id,$par){
    
               
                $rez = Yii::$app->db->createCommand('UPDATE general_settings SET usinfo_job_place = :par WHERE info_user_id=:user_id')->bindValues([':user_id'=>$user_id,':par'=>$par])->execute(); 
           return $rez;
    
}



        
public function set_user_id($user_id){
    
               
                $rez = Yii::$app->db->createCommand('INSERT INTO general_settings(info_user_id) VALUES (:user_id) ')->bindValues([':user_id'=>$user_id])->execute(); 
           return $rez;
    
}

       public function getuserid($user_id){
        
          $rez=Yii::$app->db->createCommand("SELECT COUNT(id) AS COUNT FROM general_settings WHERE info_user_id=:user_id")->bindValues([':user_id'=>$user_id])->query()->read();
          return $rez['COUNT'];
    }
    
    public function get_all_info($user_id){
           $rez=Yii::$app->db->createCommand("SELECT * FROM general_settings WHERE info_user_id=:user_id")->bindValues([':user_id'=>$user_id])->query()->read();
          return $rez;
        
    }
}
