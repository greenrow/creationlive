<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Project_foto extends \yii\db\ActiveRecord {
    
    public $path;
    public $position;
    
    
    
       public function setfoto($path,$pos,$proj_id){
        
        Yii::$app->db->createCommand("INSERT INTO Project_foto(path,position,project_id) VALUES(:path,:pos,:proj_id)")->bindValues([':path'=>$path,':pos'=>$pos,':proj_id'=>$proj_id])->execute(); 
        
}
}
