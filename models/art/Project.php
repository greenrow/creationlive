<?php

namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Project extends \yii\db\ActiveRecord {
    

    public function projset($ava,$name,$bckg,$user_id,$date){

               Yii::$app->db->createCommand('INSERT INTO Project(name,ava,background,user_id,date) VALUES(:name,:ava,:bckg,:user_id,:date)')->bindValues([':name'=>$name,':ava'=>$ava,':bckg'=>$bckg,':user_id'=>$user_id,':date'=>$date])->execute(); 
     
}


    public function proj_id($user_id){
        
       $max = Yii::$app->db->createCommand('SELECT max(id) as max FROM project WHERE user_id=:user_id')->bindValue(':user_id',$user_id)->queryScalar();
   return $max;
}


        public function get_project_files($user_id){

           $max = Yii::$app->db->createCommand('SELECT*  FROM project WHERE user_id=:user_id')->bindValue(':user_id',$user_id)->query()->readAll(); 
           return $max;

        }


        public function get_projectfoto_details($proj_id,$user_id){

           $max = Yii::$app->db->createCommand('SELECT f.position,f.path FROM project p,project_foto f WHERE p.id=:proj_id AND p.user_id=:user_id AND f.project_id=p.id  ORDER BY f.position ASC')->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->query()->readAll(); 
           return $max;

        }
        
           public function get_projectarea_details($proj_id,$user_id){

           $max = Yii::$app->db->createCommand('SELECT a.position,a.textarea FROM project p,project_area a WHERE p.id=:proj_id AND p.user_id=:user_id AND a.proj_id=p.id  ORDER BY a.position ASC')->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->query()->readAll(); 
           return $max;

        }
        
        public function get_project_info($proj_id,$user_id){
            
             $max = Yii::$app->db->createCommand('SELECT * FROM project p  WHERE p.id=:proj_id AND p.user_id=:user_id ')->bindValues([':user_id'=>$user_id,':proj_id'=>$proj_id])->query()->read(); 
           return $max;
        }
        
   

        public function publish_proj($proj_id){
            
                $max = Yii::$app->db->createCommand('UPDATE project SET publish =1 WHERE id=:proj_id')->bindValues([':proj_id'=>$proj_id])->execute(); 
           return $max;
            
        }
        
           public function find_publish($proj_id){
            
                $max = Yii::$app->db->createCommand('SELECT publish FROM project WHERE id=:proj_id')->bindValues([':proj_id'=>$proj_id])->query(); 
           return $max;
            
        }
        
                public function get_all_publish_proj(){
            
                $max = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date FROM project p,user u WHERE u.id=p.user_id AND p.publish=1')->query()->readAll(); 
           return $max;
            
        }
        
           public function get_all_publish_proj_by_cat($cat_name){
                $max = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u, category cat,categ_projects catp WHERE u.id=p.user_id AND p.publish=1 AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id ')->bindValues([':cat_name'=>$cat_name])->query()->readAll(); 
           return $max;
            
        }
        
        public function get_all_proj_by_location($loc){
            
                    $max = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u,map_coords map WHERE u.id=p.user_id AND map.user_id=p.user_id AND map.location=:loc')->bindValues([':loc'=>$loc])->query()->readAll(); 
           return $max;
            
        }
        
         public function get_all_proj_by_date($date){
            
                    $max = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u WHERE u.id=p.user_id  AND p.date=:date')->bindValues([':date'=>$date])->query()->readAll(); 
           return $max;
            
        }
        
         public function get_all_proj_by_curdate(){
            
                    $max = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u WHERE u.id=p.user_id  AND p.date=CURDATE()')->query()->readAll(); 
           return $max;
            
        }
        
        public function get_all_proj_by_week(){
            
                    $max = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u WHERE u.id=p.user_id  AND YEAR(p.date)=YEAR(NOW()) AND WEEK(p.date)=WEEK(NOW())')->query()->readAll(); 
           return $max;
            
        }
        
       public function get_all_proj_by_month(){
            
                    $max = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u WHERE u.id=p.user_id  AND YEAR(p.date)=YEAR(NOW()) AND MONTH(p.date)=MONTH(NOW())')->query()->readAll(); 
           return $max;
            
        }
        
        
     public function get_all_publish_proj_by_cat_and_loc($cat_name,$locname){
                $rez = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u, category cat,categ_projects catp,map_coords map WHERE u.id=p.user_id AND p.publish=1 AND p.id=catp.proj_id AND cat.name=:cat_name AND cat.id=catp.categ_id  AND  map.user_id=p.user_id AND map.location=:loc')->bindValues([':cat_name'=>$cat_name,':loc'=>$locname])->query()->readAll(); 
           return $rez;
            
        }
        
            public function     get_all_proj_by_month_and_loc($locname){
                $rez = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u,map_coords map WHERE u.id=p.user_id AND p.publish=1   AND  map.user_id=p.user_id AND map.location=:loc AND YEAR(p.date)=YEAR(NOW()) AND MONTH(p.date)=MONTH(NOW())')->bindValues([':loc'=>$locname])->query()->readAll(); 
           return $rez;
            
        }
        
        
        public function get_all_proj_by_calendar_and_loc($loc,$date){
           $rez = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u,map_coords map WHERE u.id=p.user_id AND p.publish=1   AND  map.user_id=p.user_id AND map.location=:loc AND p.date=:date')->bindValues([':loc'=>$loc,':date'=>$date])->query()->readAll(); 
           return $rez;
            
            
        }
        
        public function get_all_proj_by_day_and_loc($loc){
           $rez = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u,map_coords map WHERE u.id=p.user_id AND p.publish=1   AND  map.user_id=p.user_id AND map.location=:loc AND p.date=CURDATE()')->bindValues([':loc'=>$loc])->query()->readAll(); 
           return $rez;  
        }
        
        public function get_all_proj_by_week_and_loc($loc){
              $rez = Yii::$app->db->createCommand('SELECT p.id,p.user_id,p.name,p.ava,p.date  FROM project p,user u,map_coords map WHERE u.id=p.user_id AND p.publish=1   AND  map.user_id=p.user_id AND map.location=:loc AND YEAR(p.date)=YEAR(NOW()) AND MONTH(p.date)=MONTH(NOW()) AND WEEK(p.date)=WEEK(NOW())')->bindValues([':loc'=>$loc])->query()->readAll(); 
           return $rez; 
            
        }
    
        
       public function del_proj_files($user_id){
            
                $rez= Yii::$app->db->createCommand('DELETE categ_projects catp,project_area pa, project_foto pf FROM project p LEFT JOIN categ_projects catp  ON p.id=catp.proj_id LEFT JOIN project_area pa  ON p.id=pa.proj_id LEFT JOIN project_foto pf  ON p.id=pf.project_id WHERE p.user_id=:user_id')->bindValues([':user_id'=>$user_id])->execute(); 
           return $rez;
            
        }
        
        public function del_proj($user_id){
            
                $rez= Yii::$app->db->createCommand('DELETE project p FROM project p WHERE p.user_id=:user_id')->bindValues([':user_id'=>$user_id])->execute(); 
           return $rez;
            
        }
        
                public function hide_proj($user_id){
            
                $rez= Yii::$app->db->createCommand('UPDATE project p SET publish=0 WHERE p.user_id=:user_id')->bindValues([':user_id'=>$user_id])->execute(); 
           return $rez;
            
        }
        
        




}