<?php namespace app\models\art;

use Yii;
use yii\base\Model;
use yii\db\Query;

class Tag{
    
        

function find_tag_count($name){
    
    
                $count=Yii::$app->db->createCommand('Select id From Tag WHERE name=:name')->bindValues([':name'=>$name])->query();
                return count($count);
    
}

              public  function settag($name){

                Yii::$app->db->createCommand("INSERT INTO Tag(name) VALUES(:name)")->bindValues([':name'=>$name])->execute(); 

        }
        
                public  function gettag_id($name){

               $id_categ= Yii::$app->db->createCommand('Select id From Tag Where name=:name')->bindValues([':name'=>$name])->query(); 
             return $id_categ;

        }
        
                 public function gettag_name($proj_id){
                $rez=Yii::$app->db->createCommand("Select tag.name FROM Tag,project_tag projtag  WHERE projtag.tag_id=tag.id AND projtag.proj_id=:proj_id")->bindValues([':proj_id'=>$proj_id])->query(); 
                return $rez;
        }
}
