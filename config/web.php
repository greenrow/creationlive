<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basidc',
    'basePath' => dirname(__DIR__.'../'),
    'bootstrap' => ['log'],
    'components' => [
        

    
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\art\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => [
       
                'class' => 'yii\db\Connection',
                'dsn' => 'mysql:host=localhost;dbname=yii',
                'username' => 'root',
                'password' => '666666',
                'charset' => 'utf8',
            
            
            
        ],
        'assetManager' => [
    'bundles' => [
        
     
        'yii\bootstrap\BootstrapAsset'=>['css'=>['css/bootstrap.css']]
            

        // you can override AssetBundle configs here
    ],
],
        'request' => [
            'cookieValidationKey' => 'ts',
            'enableCsrfValidation' => false,
        ], 
    ],   'defaultRoute' => 'site/index',
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
