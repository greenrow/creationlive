## **Web Application Structure of Creationlive site** ##

###** Backend** ###
* Web server Apache [php]
* Socket server [python]
* Framework YII2 [php]
* Database MYSQL

### **Frontend** ###
* Javascript/Jquery [70%/30%]
* GoogleMap js api
* GoogleCharts js api
* Websocket
* Jquery plugins
* Svg animation lib
* FileLoader lib [*my own liblary written on Vanilla js*]