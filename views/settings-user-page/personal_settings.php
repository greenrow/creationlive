<?php 

$this->registerJsFile('http://maps.googleapis.com/maps/api/js?key=AIzaSyDCwmNsO67x5VPyYEKfTEGUHU--6AfVtyQ&sensor=FALSE&libraries=places',['position'=>Yii\web\View::POS_END,'depends' =>'yii\web\YiiAsset']);
?>
      <style>
    #map-canvas {
        height: 200px;
        margin: 0px;
        padding: 0px;
      
          width:100%;
    
      }


      #user_city {
        background-color: #fff;
        padding: 0 11px 0 13px;
        width: 400px;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        text-overflow: ellipsis;
      }

    #user_city:focus {
        border-color: #4d90fe;
        margin-left: -1px;
        padding-left: 14px;  /* Regular padding-left + 1. */
        width: 401px;
      }

      .pac-container {
        font-family: Roboto;
      
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }


    </style>   

    
    <div class="col-md-10 col-md-push-1 main_settings_menu ">
        <div check_it='1' id='user_settings_menu' class='col-md-2'>
            Личные настройки
        </div>
        
          <div id='general_settings_menu' class='col-md-2'>
            Общие настройки
        </div>
    </div>
       
       
<div class='col-md-8 col-md-push-2 user_info_settings'>
                    

                <div class='row input_user_info'>
                    

                              <div class='col-md-6'>
                                  <div class=" col-md-12 input-group user-setting_input">
                                               <span class=" input-group-addon " >Имя</span>
                                                <input name='user-name'  value="<?php echo $userinfo_arr['name'] ?>" type="text" id="user_name" class="form-control" aria-describedby="basic-addon2">
                                 </div>
                             </div>

                             <div class='col-md-6'>

                                <div class=" col-md-12 input-group user-setting_input">
                                       <span class=" input-group-addon " >Фамилия</span>
                                        <input name='user-surname' type="text" id="user_surname" class="form-control"   value="<?php echo $userinfo_arr['surname'] ?>" aria-describedby="basic-addon2">
                                </div>
                            </div>

                            <div class='col-md-6'>

                                   <div class=" col-md-12 input-group user-setting_input">
                                        <span class=" input-group-addon " >Возраст</span>
                                         <input name='user-age' type="text" id="user_age" value="<?php echo $userinfo_arr['age'] ?>" class="form-control"  aria-describedby="basic-addon2">
                                    </div>

                            </div>
                            <div class='col-md-6'>

                                <div class=" col-md-12 input-group user-setting_input">
                                    <span class=" input-group-addon " >Место работы</span>
                                     <input type="text" name='user-job' id="user_job" class="form-control"  value="<?php echo $userinfo_arr['job_place'] ?>" aria-describedby="basic-addon2">
                                 </div>

                            </div>



                             <div class='col-md-6'>

                                 <div class=" col-md-12 input-group user-setting_input">
                                        <span class=" input-group-addon " >Веб сайт</span>
                                         <input name='user-website' type="text" value="<?php echo $userinfo_arr['website'] ?>" id="user_website" class="form-control"  aria-describedby="basic-addon2">
                                 </div>
                            </div>
                    
                               <div class='col-md-6'>

                                 <div class=" col-md-12 input-group user-setting_input">
                                        <span class=" input-group-addon " >Email</span>
                                         <input name='user-email' type="text" value="<?php echo $userinfo_arr['email'] ?>" id="user_website" class="form-control"  aria-describedby="basic-addon2">
                                 </div>
                            </div>

                        </div>
                    
                    
                    
            
                                      
                <div class='col-md-6 interes_div'>
                        <span style='color: #555;'>Ваши интересы (введите текст и нажмите Enter)</span>
                          <input id="tag1" value='' data-role="tagsinput" class="form-control" />
                              <p style='position:absolute;left:-9999px;'class='hidden_interes'><?php echo $json_iners_arr?></p>
                </div>        
                         
                <div id="location_map" class="col-md-6 map ">            
                 
                        <div class="col-md-12">

                         <input  id="user_city" class="controls col-md-8 col-md-push-1" type="text"  placeholder="Укажите ваш город">

                             <div  class='col-md-10 col-md-push-1 map_wrap' >    
                                 <span class="hidden" id="lat_val"></span> <span class="hidden" id="lng_val"></span><span class="hidden" id="location_val" ></span>
                              <div style='height:300px;'id="map-canvas"></div>
                            </div>

                            <?php 
                               if ($mapcoord !=' '){
                                    foreach($mapcoord as $key){
                                           ?><span class="hidden lat_val"><?php echo $key['lat']?></span> 
                                           <span class="hidden lng_val"><?php echo $key['lng']?></span><span class="hidden location_val" ><?php echo $key['location']?></span><?php

                                    };

                               };
                               ?>

                        </div>
                </div>

                <div class='col-md-2 col-md-push-10'>
                    <a href='#' class='btn btn-primary save_user_info'>Сохранить</span></a>
                </div>

  </div>
    
    
    


<div class='col-md-8 col-md-push-2 general_settings'>
    <h3>Проекты</h3>

  
        <p><input name='proj'  value='hide' type='radio'/> Сделать все проекты неактивными </p>
        <p><input name='proj' value='del' type='radio'/> Удалить все проекты </p>
    

    <h3>Информация отображаемая в Проектах</h3>

    <div class="project_info_settings">
        <p><input name='email' type='checkbox'/> Email </p>

        <p><input name='website' value='tel' type='checkbox'/> Сайт</p>
        <p><input name='age' value='age' type='checkbox'/> Возраст</p>
        <p><input name='job_place' value='job_place' type='checkbox'/> Работа</p>
        <p><input name='aboutme' value='aboutme' type='checkbox'/> Интересы</p>
    </div>
    <div class=" btn btn-primary col-md-1 col-md-push-7 save_general_settings">
        Сохранить
    </div>

</div>
    
