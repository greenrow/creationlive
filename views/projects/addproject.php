  <?php

use app\assets\Project_asset;

Project_asset::register($this);
use yii\helpers\Url;
?>

     
    
    
<div class="add_project_wrap col-md-10 col-md-push-1">
        
                <!--Обложка проекта-->
    <div class="col-md-12 project_nav_wrap">
    <div class='project_nav col-md-6 '>
        <div data-step='0'  class='add_ava'>
               <div class="col-md-12 project_nav_svg">
                <object id='svgnav_0' width='100' height='50' type='svg/xml' data="/images/svg/project_nav_line.svg"></object>
            </div>
            <div class="project_nav_title">
                <p>Шаг 1</p>
            </div>
           
        </div>
        <div data-step='1' class='add_proj_content  '>
              <div class="col-md-12 project_nav_svg">
                <object   width='100' height='50' type='svg/xml' data="/images/svg/project_nav_line.svg"></object>
            </div>
            <div class="project_nav_title">
                 Шаг 2
            </div>
        </div>
        <div data-step='2' class='publish_proj'>
                   <div class="col-md-12 project_nav_svg">
                <object width='100' height='50' type='svg/xml' data="/images/svg/project_nav_line.svg"></object>
            </div>
            <div class="project_nav_title">
                 Шаг 3
            </div>

        </div>

        
    
    </div>
     <div class='col-md-1 col-md-push-3 next_add_project_item'>
         <div class='next_add_project_item_back col-md-12'></div>
            Далее
        </div>
        
        <div class='saveproject_div col-md-6'>
            
            <div class='preshow_proj col-md-3'>
              <span class='glyphicon glyphicon-eye-open'></span><br/>
                        
                         Просмотреть
            </div>
                <div class='col-md-4' id="submit_project">
                    <span class='glyphicon glyphicon-floppy-disk'></span><br/>
                        Сохранить
                    </div>
                <div class='col-md-4' id="submit_publish">
                    <span class='glyphicon glyphicon-ok'></span><br/>Опубликовать
                    </div>
        </div>
    </div>
             
     <div class='project_canvas col-md-12'>

         <div  class='project_add_ava'>

                                
             <div class='col-md-3 col-md-push-1 project_ava'>
              
                  <div class='u_title_proj_pic_main col-md-10'>
                         <p style='text-align:center;'>Обложка проекта</p>
                      <img  class=' col-md-12 img-rounded img-responsive'  src=""/>
                  </div>

                   <div class=' btn btn primary change_title_proj_foto col-md-9'>
                              <p>Сменить фото</p>
                      </div> 

             </div> 
             
             <div class="col-md-6 col-md-push-2 project_title_desc">
                     <p> Название <input name="project_title" type='text'  class='col-md-12 current_proj_title'/></p>
                  
                          <p>Описание  <textarea rows='5' name='project_desc' class='col-md-12 proj_desc'></textarea></p>
             </div>
            
            
            <div class='show_project_title_foto col-md-5 col-md-push-5' >  

                             <p class='close_ava_add_div' style='text-align:right;color:red;font-weight:600'>
                                 <span class='glyphicon glyphicon-remove '><span></p>

                    <div class='u_title_proj_pic col-md-6'>

                            <img  class=' col-md-12 img-rounded img-responsive' src=""/>
                   </div>

            <div class='col-md-12'>
                    <div class='col-md-6  file_upload'>
                                   <span  style='width:100%;' class="glyphicon glyphicon-download" >
                                    <input class='u_project_tiltle' type="file"> Загрузить </span>  
                    </div>
            </div>

                      <div class='col-md-12'>                              
                            <p class='col-md-4 col-md-push-8 save_title_proj_foto' style='cursor:pointer;text-align:center;margin-top:40px;background-color:#5EBEFF;color:#fff;padding:10px;'>Сохранить</p>
                     </div>

             </div>

                     
        
        </div>
        

        
                        <!--содержимое проекта-->
                
            <div  class="add_project_content">
                 <div class="col-md-10 col-md-push-1  top_add_project_menu" >
               
                     <div class="project_foto_upload col-md-2">
                         <input class='project_foto' type="file"> Фото<br/><span style='color:#AA2525;font-size:1.1em;' class="glyphicon glyphicon-picture"></span> </input></div>
                     <div  id="proj_text" class='col-md-2'>  Текcт <br/><span style='color:#AA2525;'class='glyphicon glyphicon-font'></span>
                     
                         </div>
                            <div class='col-md-2' id="embed_video"> Встроенное видео<br/><span style='color:#AA2525;'class='glyphicon glyphicon-chevron-left'></span>
                          
                                <span style='color:#AA2525;' class='glyphicon glyphicon-chevron-right'></span>
                            </div>



                            <div class='change_pos col-md-2'>Изменить положение <br/><span style='color:#AA2525;' class="glyphicon glyphicon-sort" ></span>
                            </div>
                            <div class='choose_background_proj col-md-2'>Выбрать фон<br/><span style='color:#AA2525;' class='glyphicon glyphicon-adjust'></span>
                            </div>


                </div>
                
                    <div class="row pickercolor_wrap col-md-6 col-md-push-5 ">
                           <div class='col-md-1 col-md-push-12' id="close_picker">
                               &times;
                           </div>
                           
                                <div id="proj_colorpicker" ></div>
                       </div>


                <div id='main_content'  class="ui-sortable col-md-10 col-md-push-1 ">

                   </div>

         </div>
                        
         <div class="additional_project_info">
                           
                <div class="row col-md-10 col-md-push-1">
    
                        <p> Категория   </p>
                    
              
                         <div class="col-md-8  add_project_cat add_selected_categ"></div>
                        
              
                     

                 <div class='add_project_category_list col-md-10'>

                     <div style='text-align:right;color:red;font-size:16px;font-weight:bold;opacity:0.8' class='col-md-1 col-md-push-11 close_main_sort_cat_list'>&times;</div>

                            <div class=" col-md-3 add_proj_categ_div">
                                <p class="categ_title">Веб</p>
                                <div class='categ_list'>
                                     <p>Веб дизайн</p>
                                     <p>Ui/Ux</p>
                                 </div>
                            </div>

                            <div class="col-md-3  add_proj_categ_div">
                                <p class="categ_title">Архитектура</p>
                                <div class='categ_list'>
                                     <p>Проектирование зданий</p>
                                     <p>Ландшафтная архитектура</p>

                                      <p>Дизайн интерьера1</p>
                                 </div>
                            </div>

                            <div class="col-md-3  add_proj_categ_div">
                                  <p class="categ_title">Живопись</p>
                                 <div class='categ_list'>


                                   <p>Потретная живопись</p>
                                   <p>Пейзаж</p>
                                </div>

                            </div>
               
                     <div style='clear:both;' class='col-md-2 col-md-push-10'>
                         <p class='save_categ'>Сохранить</p>
                     </div>
                   

                </div>

     
                     
                </div>
                        
            <div class="row col-md-8 col-md-push-2">
        
                      <p> Теги </p>
                

                 <div class='col-md-8 tag_div'>
                        <span style='color: #555;'>Ваши теги(введите текст и нажмите Enter)</span>
                        <input id="tag_proj" value='' data-role="tagsinput" class="form-control" />

                </div> 
            </div>
                           
          </div>

           
      
 

    
</div>
              
</div>

