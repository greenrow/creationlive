<style>

    .main-news h3{text-align:center;margin:10px 0px ;font-weight:bold;}
   .main-news right_text{text-align:right}
     .main-news div {float:left;padding:5px;margin:10px 0px}
     .full_org_name{width:100%;}
     .bord_btm{border-bottom:1px solid black;}
     .txt_centr{text-align:center;}
     .i_am{width:10%}
     .fio {width:80%}
     .ustav{width:20%}
     .ustav_text{width:70%;clear:right;}
     .main_tab{border:1px solid black;width:100%;border-collapse: collapse;}
       .main_tab td{width:50%;border:1px solid black;padding:5px;}
         .ruk_sign_wrap,.ruk_fio_wrap {width:28%}
.ruk_fio_wrap{width:35%}
         .ruk_print{width:5%;margin:0px 2%}
.main-news{width:100%}
.right_text{text-align:right;}

  .footer div,.top_wrap div{min-height:20px;height:auto}
  .ruk_fio,.ruk_sign{width:100%}
  .ruk_fio_wrap{clear:right}
     
</style>


<div class="main-news">
    
    <h3> ФОРМА
заявления на аккредитацию на торговой площадке</h2>
    
    <p class="right_text">Удостоверяющий центр 
ФГУП «ЦентрИнформ»</p>
    
    <p class='txt_centr'>Заявление <br/>
на аккредитацию на торговой площадке<p>
    <div class='top_wrap'>
    <div class='full_org_name bord_btm'>%FullName%</div>
    <p style='padding-left:100px;' class='txt_centr'><sup>(полное наименование организации, включая организационно-правовую форму)</sup></p>
    <div class='i_am'>в лице</div>
    <div class='fio bord_btm'>%RukFIO%</div>
<p  style='clear:both;padding-left:250px;' class='txt_centr><sup >(должность, фамилия, имя, отчество)</sup></p>

    <div class='ustav'>действующего на основании</div>
    <div class='ustav_text bord_btm'>
        %UstRod%
    </div> 
    
    <div style="width:100%;clear:both">
        <p style='margin-top:25px'>Просит произвести аккредитацию организации на торговой площадке в соответствии с указанными в настоящем заявлении данными:   </p>
    </div>  
        <table class='main_tab'>
            <tr>
                <td>
                   Наименование организации  / ФИО 
                </td>
                 <td>
                    %FullName%
                </td>
            </tr>
            
           <tr>
                <td>
                    Город 
                </td>
                 <td>
                    %EcpGorod%
                </td>
            </tr>
            
            <tr>
                <td>
                   Область  
                </td>
                 <td>
                    %EcpRegionFull%
                </td>
            </tr>
            
            <tr>
                <td>
                   РФ 
                </td>
                 <td>
                    
                </td>
            </tr>
            
            <tr>
                <td>
                   Адрес электронной почты (обязательно) 
                </td>
                 <td>
                    %EcpEmail%
                </td>
            </tr>
            
           <tr>
                <td>
                   ИНН / КПП / ОГРН 
                </td>
                 <td>
                    %OrgINN% /%OrgKPP% /%OGRN%
                </td>
            </tr>
            
            <tr>
                <td>
                  ЮЛ, Индивидуальный предприниматель  
                </td>
                 <td>
                    
                </td>
            </tr>
            
            <tr>
                <td>
                   Тип регистрации 
                </td>
                 <td>
                    
                </td>
            </tr>
            
            <tr>
                <td>
                    Адрес банка
                </td>
                 <td>
                    
                </td>
            </tr>
            
            <tr>
                <td>
                 Наименование торговой площадки   
                </td>
                 <td>
                    
                </td>
            </tr>
            
            <tr>
                <td>
                   ОКАТО 
                </td>
                 <td>
                    
                </td>
            </tr>
            
            <tr>
                <td>
                    Контактный телефон
                </td>
                 <td>
                    %Tel%
                </td>
            </tr>
        </table>
               
        
    </div>
    
    <p>Список необходимых документов прилагаю.</p>
    <div style='clear:both;width:100%;' class='footer'>
    <div class='ruk_fio_text'>Руководитель организации</div>
    <div class='ruk_sign_wrap'>
        
        <div class='ruk_sign bord_btm'>
            
        </div>
           <p class='txt_centr'><sup>(подпись) </sup></p>
    </div>
    
    <div class='ruk_print'>М.П.</div>
    
    <div class='ruk_fio_wrap'>
  
        <div class='ruk_fio bord_btm'>
                  %RukFIO% 
        </div>
        
        <p class='txt_centr'><sup>          (фамилия,  инициалы)</sup></p>
    </div>
    </div>
     <p style='clear:both;'>«_______» _________________________ 201   г.	</p>
   
</div>