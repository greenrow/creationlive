<?php

use yii\helpers\Html;

use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\SecAsset;

use yii\helpers\Url;


/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
SecAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
    

</head>
<body>

<?php $this->beginBody() ?>
  
        
   

            <header class='col-md-12 main_top_header'>

                  <nav class='col-md-8 col-md-push-4  main_top_header_nav'>
                        <div class="col-md-1">
                            <a class='a_menu' href='<?php echo Url::toRoute(['/site/']);?>'>Главная</a>
                        </div>
                      
                        <div class="col-md-1">
                            <a class='select_main_cat a_menu' >Коллекции</a>
                        </div>
            


                                      <?php if(Yii::$app->user->isGuest){?>
                            <div class="col-md-1">
                                <a class='a_menu user_login'>Войти </a>

                            </div>

                                   <?php }else{?>
                      <p id='chek_if_user_is_not_guest' login='login_true'></p>
                      
            <div class=" col-md-8 header_user_menu" >
                      <div class="col-md-2 col-md-offset-3 user_ava_main_page_wrap">
                                                  
                            <a class='a_menu user_pers_login'><?php echo Yii::$app->user->identity->login;?></a>
                            <div class='user_ava_main_page'>
                                      <img class='img-circle'/>      
                            </div>
                      </div>
     
         
       
                
                
                    <div class="user_popup_menu_wrap col-md-2 col-md-offset-1">
                    
                        <span class='user_popup_menu_icon glyphicon glyphicon-list'></span>
                         <div class="user_pers_popup_menu col-md-12">
            
                                <div class="popup_uprofile">   

                                   <span class="glyphicon glyphicon-user"></span>
                                   <a href="<?php echo Url::to(['/projects'])?>">Профиль</a>
                                       </div>


               <div class="popup_uprofile">   
                                         <span class=" glyphicon glyphicon-info-sign"></span>
                                        <a href="<?php echo Url::to(['personalpage/getava'])?>">О проекте</a>
                                    </div>

                  

                                   <div class="popup_exit">   <span class="glyphicon glyphicon-off">

                                         </span><a href="<?php echo Url::to(['/site/logout/'])?>">Выход</a> 
                                   </div>    
                        </div>
                  
                    </div>
              
   
                    
               </div>
                       <div class="current_time col-md-2">
                    <p class="current_time_php"></p>

                </div>

                          <?php } ?>
                      </nav>
                
            
            </header>

   

    
        <div class='project_category_main col-md-4 col-md-push-4'>

              <div style='text-align:right;color:red;font-size:16px;font-weight:bold;opacity:0.8' class='col-md-1 col-md-push-11 close_main_sort_cat_list'>&times;</div>

               <div class="categ_div_main">
                   <p class="categ_title">Веб</p>
                   <div class='categ_list'>
                        <p>Веб дизайн</p>
                        <p>Ui/Ux</p>
                    </div>
               </div>

               <div class="categ_div_main">
                   <p class="categ_title">Архитектура</p>
                   <div class='categ_list'>
                        <p>Проектирование зданий</p>
                        <p>Ландшафтная архитектура</p>
                     
                         <p>Дизайн интерьера1</p>
                    </div>
               </div>

               <div class="categ_div_main">
                     <p class="categ_title">Живопись</p>
                    <div class='categ_list'>


                      <p>Потретная живопись</p>
                      <p>Пейзаж</p>
                   </div>

               </div>

        </div>

      
             

       

            
           
   

     

  <div class="row col-md-12 medium_main_area">
                  <div id ='sv' class='logo col-md-2'>
                      <div class="logo_text col-md-8">
                          <span>CREATION</span>
                      </div>
                    <div class="col-md-3 rotate_logo_text">
                        <span>LIVE</span>
                    </div>
                </div>
    <div class="col-md-push-3 project_nav_sort_menu ">
          <div>
              <span class="glyphicon glyphicon-sort"></span> 
          </div>
 
      </div>
      
          <div class="filter_svg_menu_wrap hide_div col-md-8 col-md-push-3">
                                          
                     <div  class="col-md-6 filter_svg_menu">
                         <div class="col-md-2 col-md-push-1 svg_like">
                             <div class=" svg_icon_wrap">
                                 
                             </div>
                             <object id='svg_like_obj' width="60px" data="/images/svg/filt_menu_icon_like.svg" type="svg/xml"></object>
                         </div>
                         
                        <div class="col-md-2 col-md-push-1  svg_view">
                             <div class="svg_icon_wrap">
                                 
                             </div>
                             <object id='svg_view_obj'  width="60px" data="/images/svg/filt_menu_icon_view.svg" type="svg/xml"></object>
                        </div>
                         
                        <div class="col-md-2 col-md-push-1  svg_date">
                             <div class=" svg_icon_wrap svg_date_div">
                                 
                             </div>
                             <object id='svg_date_obj'   width="60px" data="/images/svg/filt_menu_icon_date.svg" type="text/svg+xml"></object>
                        </div>
                         <div class="col-md-2 col-md-push-1  svg_loc">
                             
                             <div class=" svg_icon_wrap svg_loc_div">
                                 
                             </div>
                             <object id='svg_loc_obj' width="60px" data="/images/svg/filt_menu_icon_loc.svg" type="text/svg+xml"></object>
                        </div>
                     </div>
                     
                     
                     <!--карта-->
                   
                         <div class='sort_map_wrap'>
                             <p class="mybtn_close"><span class="btn_content"></span></p>
                                <p class=" clearboth"><input class='autocomplete_input col-md-12' type="text" value=''/></p>
                                    <div class="col-md-10 col-md-push-1  sort_map">
                                    </div>
                                <div class=" col-md-10 error_div">
                                    
                                </div>
                                <p class=" clearboth sort_select_proj_loc mybtn col-md-3 col-md-push-8">
                                    Найти
                                </p>
                        </div>
                     
                     <!--calendar-->
                     
                        <div class='project_date_check'>
                                 <p class="mybtn_close"><span class="btn_content"></span></p>
                       
                          
                                <ul class="proj_sort_menu col-md-12">
                                    <li class="col-md-12 proj_date_sort" data-date="thisday">За текущий день</li>
                                    <li class="col-md-12 proj_date_sort" data-date="week">За неделю</li>
                                    <li class="col-md-12  proj_date_sort" data-date="month">За месяц</li>
                                    <li class="col-md-12 proj_date_calendar" data-date="calendar">Выбрать дату</li>
                                </ul>
                                
                        
                             <div class="calendar_data">
                                 
                             </div>
                       
                        </div>
                     
      
                      
                
          <div class='project_sort_info col-md-6'>
          
              
            <div class="col-md-3 sort_project_date">
                      
                 <div class="opacity_wrap">
                      
                  </div>
                <div class="sort_wrap">
                    <p>Дата<span class="close_date_sort">x</span></p>
                    <div class="project_time"></div>
                </div>
             </div>
              
              <div class="col-md-3 sort_project_loc">
                  <div class="opacity_wrap">
                      
                  </div>
                  <div class="sort_wrap">
                    <p>Локация<span class="close_loc_sort">x</span></p>
                    <div class="map_project_city"></div>
                  </div>
              </div>
                          
          </div>
                

   
                
                
           </div>

      
           <div class='col-md-2 user_profile_top_right_fon col-md-push-11'></div>
             
            
              <div class='main_page_categ_view col-md-9 '>
                     <div  class=' col-md-push-3 col-md-4 col-lg-4 selected_main_categ_list'></div>
                </div>
           <div class='col-md-push-4 col-md-4' id="loginform"><
               
               </div>
              
           
           <div class='col-md-2 user_profile col-md-push-10'>
               
           </div>
    </div>
        <div class="project_detail_show_wrap col-md-12">
                            <p class='close_curent_project'><strong>Закрыть</strong></p>
                            
                <div class='project_detail_show col-md-5  col-md-offset-2'>
      


                </div>

               <div class='col-md-2 col-md-push-1'>

                   <div class="project_user_avatar col-md-11">

                   </div>
                   <div class='col-md-11 project_detail_user_info '>

                   </div>
                    <div class='col-md-11 project_category_info '>

                   </div>

                </div>

        </div>

        <div class="container col-md-12 main-page_layer">

            
            

                        <?= $content ?>
        </div>



    <?php 



$this->registerJsFile('http://maps.googleapis.com/maps/api/js?key=AIzaSyDCwmNsO67x5VPyYEKfTEGUHU--6AfVtyQ&sensor=FALSE&libraries=places',['position'=>Yii\web\View::POS_END,'depends' =>'yii\web\YiiAsset']);

?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
