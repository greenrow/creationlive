<?php
namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\art\Project;
use app\models\art\Category;
use app\models\art\Categ_projects;
use app\models\art\Project_foto;
use app\models\art\Project_area;
use app\models\art\Likes;
use app\models\art\Userinfo;
use app\models\art\Map_Coords;
use app\models\art\General_settings;



class ProjectsController extends Controller

{
    
        //массив информации о проекте//
        public $showproj=[];
        
        public $layout = 'projects';
 
        public function actionIndex(){
                $proj= new Project;
                $ava= $proj->get_project_files(Yii::$app->user->getid());
                return $this->render('index',['ava'=>$ava]);
        }
        
        
   public function actionGetallprojects(){
        $proj= new Project;
        
          $categ_arr=[];
 
          
                $rez=$proj->get_all_publish_proj();

        foreach($rez as $key){  
            
             $this->publish_project_info['project_all'][$key['id']]= $key;
            
              $categ=$this->get_cur_proj_cat($key['id']);
                    
               $this->publish_project_info['project_all'][$key['id']]['category']=  $categ;

          //очищяем массив
         $categ_arr=array();  
        }
               
       return json_encode($this->publish_project_info);
    }
    
    
    

        public function actionAddtitleptoj(){
                $avatarload=new Userpage;
                $ava_get=$avatarload->getAvatar();
                if($ava_get==''){
                        $ava_get='/images/no_foto.png';
                }
                return   $ava_get;
        }

        public function actionTemplateaddproject(){
                return $this->renderAJAX('addproject');
        }

        public function actionProjects(){
                return $this->render('projects');
        }

        public function actionAddfoto(){
                $filetype=[];
                $filedir= 'uploads/'.Yii::$app->user->getid().'/temp/';
                if(!is_dir($filedir)){
                        mkdir( $filedir);
                }
                $type=$_FILES['ufile']['type'];
                
                if($type=='image/png' || $type=='image/jpeg' ){
                        $image_type=substr($_FILES['ufile']['name'],-3);
                        $file_date_name=date('dmy').'_'.rand(100,1000).'.'.$image_type;
                        $full_filename="$filedir$file_date_name";
                        $image=$_FILES['ufile']['tmp_name']; 
                        $uploadfile=  move_uploaded_file($image,$full_filename);
                        
                        if($uploadfile){
                                $filetype['foto']=$full_filename;
                                return  json_encode($filetype);
                        }
                }
        }
    

        public function actionAddproj(){
                $name=$_POST['name'];
                $ava=$_POST['ava'];
                $bckg=$_POST['bckg'];
           
               $date= sprintf('%04d,%02d,%02d',date('Y'),date('m'),date('j'));
    
                $proj= new Project;
                $proj->projset($ava,$name,$bckg,Yii::$app->user->getid(),$date);
                return   $proj-> proj_id(Yii::$app->user->getid());
        }

        
        public function actionPublish_proj(){
                $proj_id=$_POST['proj_id'];
                $proj= new Project;
                $proj->publish_proj($proj_id);
        }
        
        public function get_cur_proj_cat($proj_id){
                $categ_project=  new Categ_projects;
            
                $category_rez=$categ_project->get_cat($proj_id);
                return      $category_rez;
        }
        
        public function get_publish_status($proj_id){
                $project=  new Project;
                $status_rez= $project->find_publish($proj_id);
                return $status_rez;
        }

                //********Записывае данные о проекте**********//
        
                            //категория//
                
        public function actionAddcategory_proj(){
                $category = new Category;
                $categ_project = new Categ_projects;
                $proj_id=$_POST['proj_id'];
                $categ_arr = $_POST['categ'];

                foreach($categ_arr as $key){
                        if($category->find_categ($key)==0){
                                $category->setcateg($key);  
                        }
                        $categ_id = $category->getcateg_id($key); 
                        foreach($categ_id as $key_id){
                                $categ_project->setcateg_proj($key_id['id'],$proj_id);
                        }
                }
        }
                
                                 //теги//
                
        public function actionAddtag_proj(){
                $category =  new Category;
                $categ_project=  new Categ_projects;
                $proj_id = $_POST['proj_id'];
                $categ_arr = $_POST['categ'];

                foreach($categ_arr as $key){
                        if($category->find_categ($key)== 0){
                                $category->setcateg($key);  
                        }
                $categ_id = $category->getcateg_id($key); 
                foreach($categ_id as $key_id){

                        $categ_project->setcateg_proj($key_id['id'],$proj_id);
                    }
                 }
        }
                            //Фото//
        public function actionAddfoto_proj(){
                $path = $_POST['path'];
                $pos = $_POST['pos'];
                $proj_id = $_POST['proj_id'];
                $proj_foto = new Project_foto;
                $proj_foto->setfoto($path,$pos,$proj_id);
        }
                            //Текст//
        public function actionAddarea_proj(){
                $text = $_POST['text'];
                $pos = $_POST['pos'];
                $proj_id = $_POST['proj_id'];
                $proj_area = new Project_area;

                $proj_area->setarea($text,$pos,$proj_id);
        }
  
            //-----**********---показываем данные проекта----**************-----//
             
                         //категории проекта//
        protected function ProjectCategory($proj_id){
                $categ = new Category; 
                $category = $categ->getcateg($proj_id);
            
                        $this->showproj['all']['category']=    $category;
                
              
        }
         
                          //личная информация//
         protected function PersonalInfo($user_id){
                $userinfo = new Userinfo;
                  $userinfo_settings= new General_settings;
 
                  $getuserinfo = $userinfo->getuserinfo($user_id);
                    
                  $userinfo_set_rez=$userinfo_settings->get_all_info($user_id);
              
                  if(count($userinfo_set_rez > 0)){
                  
                        foreach($userinfo_set_rez as $key=>$value){
                           if ($value == 1){
               
                               /*убираем название  usinfo_ чтобы ключ сопадал с название поля из таблицы userinfo*/
                               $key=substr($key,7);
                         
                                $this->showproj['all']['userinfo'][$key]= $getuserinfo[$key];
                                  
                                }
                        }
                        
                        /*дополняем именем фамилией и авой*/
                        
                          $this->showproj['all']['userinfo']['surname']=$getuserinfo['surname'];
                                   $this->showproj['all']['userinfo']['name']=$getuserinfo['name'];
                                     $this->showproj['all']['userinfo']['file']=$getuserinfo['file'];

                  }else{

                                $this->showproj['all']['userinfo'] = $getuserinfo;
                        
                  }
          
        }
                      //личная локация пользователя//
        protected function PersonalLocation($user_id){
                $location = new Map_Coords;
                $getlocation = $location->select_map_coord($user_id);
                foreach($getlocation as $key){
                        $this->showproj['all']['location'] = $key;
                }
        }
         
        protected function PersonalProject($proj_id,$user_id){
                $proj = new Project;
                $foto = $proj->get_projectfoto_details($proj_id,$user_id);
                $area = $proj->get_projectarea_details($proj_id,$user_id);
           
                            //если в проекте есnь фото и текст//
                if(count($foto)>0 && count($area)>0){
                        foreach($foto as $key){
                                $this->showproj['all']['project'][] = $key;
                        }
                        foreach($area as $key){
                                $this->showproj['all']['project'][] = $key;
                        }
                        array_multisort($this->showproj['all']['project'], SORT_ASC );
                }
                       
                if(count($foto)>0 && count($area) == 0){
                        foreach($foto as $key){
                                $this->showproj['all']['project'][]=$key;
                        }
                
                }
                       
                if(count($area)>0 && count($foto)==0){
                        foreach($area as $key){
                                $this->showproj['all']['project'][]=$key;
                        }
           
                }
         }
 
        public function actionShowproj(){
                $proj_id = $_GET['proj_id'];
                $user_id = $_GET['user_id'];
           
               //отображаем категории//
                $this->ProjectCategory($proj_id);
       
              //отображаем личную информацию//
        $this->PersonalInfo($user_id);
             
            //отображаем локацию пользователя//
                $this->PersonalLocation($user_id);
             
             //отображаем содержимое проекта//
                $this->PersonalProject($proj_id,$user_id );
                               
                //возвращаем масив данных о проекте//
             return json_encode($this->showproj);
        }    
    
        public function actionGetlocation(){
                $lat = htmlspecialchars($_GET['lat']);
                $lng = htmlspecialchars($_GET['lng']);
                $location = htmlspecialchars($_GET['loc']);
        
                return $this->renderAjax('location',['lat'=>$lat,'lng'=>$lng,'loc'=>$location]);
        }

        public function actionLikesit(){
               $proj_id = htmlspecialchars($_POST['proj_id']);
               $user_id = htmlspecialchars($_POST['user_id']);
               $likes = new Likes;
               $likes->find_user_likes($proj_id,$user_id);
               $rez = $likes->get_likes($proj_id);
               return $rez;
        }
    
        public function getlikes_count($proj_id){
                $likes = new Likes;
                $likesCount = $likes->get_likes($proj_id);
                return $likesCount;
        }
        
        
        public function actionGetava(){

            $avatarload=new Userinfo;

             $ava_get=$avatarload->getAvatar();
            if($ava_get=='')
             {
                 $ava_get='/images/no_foto.png';

             }
            return $ava_get; 
         
}


}