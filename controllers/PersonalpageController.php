<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\art\Map_coords;
use app\models\art\Interests;

use app\models\art\Userinfo;



class PersonalpageController extends Controller{
    

     public $layout = 'main';    

public function actionIndex(){
       
   return $this->render('index');
         
}

public function actionGetava(){

            $avatarload=new Userinfo;

             $ava_get=$avatarload->getAvatar();
            if($ava_get=='')
             {
                 $ava_get='/images/no_foto.png';

             }
           if(Yii::$app->request->isAjax){
                  if($_GET['userava']){ 
                      return   $ava_get;
                              
                  };
                  
              
              }
              
                         if(Yii::$app->request->isGet || Yii::$app->request->isPost){
        
                    $userlogin= Yii::$app->user->identity->login;
                  return $this->render('userdiv',['ava'=>$ava_get,'login'=>$userlogin]);
              }     
         
}




public function actionLoadava(){      
    
              $avatarload=new Userinfo;
    if(Yii::$app->request->post()){
         $file=Yii::$app->request->post('filepath');
         
          
            $ava_set=$avatarload->avatar_load($file);
            
        }}


public function actionLoadfoto(){
          $filedir= 'uploads/'.Yii::$app->user->getid().'/temp/';
      $type=$_FILES['ufile']['type'];
      if($type=='image/png' || $type=='image/jpeg' ){
      
            $image_type=substr($_FILES['ufile']['name'],-3);

             $file_date_name=date('dmy').'_'.rand(100,1000).'.'.$image_type;


                  $full_filename="$filedir$file_date_name";

             $image=$_FILES['ufile']['tmp_name']; 
             $uploadfile=  move_uploaded_file($image,$full_filename);
             if($uploadfile){
            $filetype['foto']=$full_filename;
                return  json_encode($filetype);
             }
             
        }else{ 
                 
                 echo 'выбран неверный тип файла';
                 
             }
    
}


}