<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\models\art\User;
use app\models\art\Project;
use app\models\art\Map_coords;

use app\models\art\Categ_projects;
use app\models\art\Likes;


class SiteController extends Controller

{
  
  public $publish_project_info=[];
    public $layout = 'main';
    

  
    public function getInfo(){
        
        
     
    }
    
    public function actionX() {
       return  $this->render('test1');
                
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login','logout','regit'],
                'rules' => [
                    
                    [
                     'actions'=>['login','register'],
                         'allow' => true,
                        'roles' => ['?'],
                        
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
      
        ];
    }

    

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
         return $this->render('index');
    }

    public function actionGetallpublishprojects(){
        
        $proj= new Project;
        
        //множественные парметры запроса//
    if(Yii::$app->request->get('loca_cat')){  //category & location//
        $cat_name=Yii::$app->request->get('cat_name');
        $loc_name=Yii::$app->request->get('loc');
        $rez=$proj->get_all_publish_proj_by_cat_and_loc($cat_name,$loc_name);
    }else if(Yii::$app->request->get('get_thismonth_and_loc')){//month&loc
        $loc=Yii::$app->request->get('loc');
        $rez=$proj->get_all_proj_by_month_and_loc($loc);   
    }else if(Yii::$app->request->get('calendar_loc')){//calendar_data&loc
        $date=Yii::$app->request->get('date');
        $loc=Yii::$app->request->get('loc');
        $rez=$proj->get_all_proj_by_calendar_and_loc($loc,$date);   
    }else if(Yii::$app->request->get('get_thisday_and_loc')){//day&loc
        $loc=Yii::$app->request->get('loc');
        $rez=$proj->get_all_proj_by_day_and_loc($loc);   
    }else if(Yii::$app->request->get('alldate_and_loc')){//allday&loc
        $loc_name=Yii::$app->request->get('loc');   
        $rez=$proj->get_all_proj_by_location($loc_name);  
    }else if(Yii::$app->request->get('get_thisweek_and_loc')){//week&loc
        $loc=Yii::$app->request->get('loc');
        $rez=$proj->get_all_proj_by_week_and_loc($loc);
        
            //единичные параметры запросы//
    }else if(Yii::$app->request->get('alldate')){ //category//
        $rez=$proj->get_all_publish_proj();
    }else if(Yii::$app->request->get('cat_name')){ //category//
        $cat_name=Yii::$app->request->get('cat_name');   
        $rez=$proj->get_all_publish_proj_by_cat($cat_name);
    }else if(Yii::$app->request->get('loc')){ //location//
        $loc_name=Yii::$app->request->get('loc');   
        $rez=$proj->get_all_proj_by_location($loc_name);
          
    }else if(Yii::$app->request->get('date')){ //date//
        $date=Yii::$app->request->get('date');   
        $rez=$proj->get_all_proj_by_date($date);
   
    }else if(Yii::$app->request->get('get_thisdate')){//current date//
        $rez=$proj->get_all_proj_by_curdate();
    }else if(Yii::$app->request->get('get_thisweek')){ //one week//
         $rez=$proj->get_all_proj_by_week();
    }else if(Yii::$app->request->get('get_thismohth')){//month
        $rez=$proj->get_all_proj_by_month();   
    
    }else{
         $rez=$proj->get_all_publish_proj();

    }
    
   
       
        foreach($rez as $key){  
            
             $this->publish_project_info['project_all'][$key['id']]= $key;
            
              $categ=$this->get_cur_proj_cat($key['id']);
                    
               $this->publish_project_info['project_all'][$key['id']]['category']=  $categ;

        }
               
       return json_encode($this->publish_project_info);
             //очищяем массив//
       
   $this->publish_project_info=array();
          
          
    }
   
          
      public function actionFupload(){
        

            /*временая зона*/

                date_default_timezone_set('Europe/Moscow');
             $image_type=substr($_FILES['ufile']['name'],-3);

              $file_date_name=date('dmy').'_'.rand(100,1000).'.'.$image_type;


                   $full_filename="$filedir$file_date_name";

              $image=$_FILES['ufile']['tmp_name']; 
              $uploadfile=  move_uploaded_file($image,$full_filename);
              if( $uploadfile){

                   return $full_filename;

              }
    }
      
    public function actionGettimezone(){
        
        $time_zone=new Map_coords;
        $timezone_rez=$time_zone->get_time_zone(Yii::$app->user->getid());
     
        date_default_timezone_set($timezone_rez);
        return Date('Y-m-d H:i:s');
        
    }

    
    public function actionLoginTest()
    {
            {

                $login=$_POST['login']['name'];
                $passw=$_POST['login']['passw'];


               $us=new User;
               $us->find_id_main($login);
                              
                 if( $us->check_login($login)==1 ){
                     
                    if($us->validate_pass($passw,$login)==1){
                        $rez= User::findIdentity($us->find_id_main($login));
                        Yii::$app->user->login($rez);
                        $filedir= 'uploads/'.Yii::$app->user->getid().'/';
                        if(!is_dir($filedir)){
                            mkdir( $filedir);
                         }
                        return $this->goHome();
                    }else{
                        echo 'passw incorrect';
                  }
                }else{
                  echo 'login incorrect';
                }

             }
             

    }

    
    


    public function actionLogin()
    {
        
        return $this->renderAjax('login');

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {

    }
    
      public function actionRegisterForm()
              
              
        {
                       return $this->render('register');

        }
    
       public function actionRegister()
       {
           
   $name=$_POST['regform']['login'];
        $us=new User;
    if( $us->check_login($name) ==0){
         $form_info=\Yii::$app->request->post('regform');
         $us->attributes=$form_info;
         $us->save();
                             echo 'you register';
               }else{
                   echo 'login busy';

               };
   
}


    


        public function actionLikesit(){
        
       $proj_id= htmlspecialchars($_POST['proj_id']);
        $likes=new Likes;
         $likes->find_user_likes($proj_id,Yii::$app->user->getid());
            $rez=$likes->get_likes($proj_id);
          return $rez;
          
    }
    
    public function getlikes_count($proj_id){
            $likes=new Likes;
          $rez=$likes->get_likes($proj_id);
          return $rez;
          
    
    }
    
          public function get_cur_proj_cat($proj_id){
                $categ_project=  new Categ_projects;
            
               $category_rez=$categ_project->get_cat($proj_id);
               return      $category_rez;
        }
        
        

            
            
public function actionTesty(){
    
    return $this->render('test');
    
}
             
 
            
    
        
        

}
    
      