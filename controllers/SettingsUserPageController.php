<?php


namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\art\Interests;
use app\models\art\Likes;
use app\models\art\Map_coords;
use app\models\art\Userinfo;
use app\models\art\General_settings;
use app\models\art\Project;


Class SettingsUserPageController extends Controller{
    
    /* обьект класса Priject*/
     private function proj(){return new Project;}
    
            public $layout='projects'; 
           

            public function getuser_id(){ return Yii::$app->user->getid();}

            
public function actionGetava(){

            $avatarload=new Userinfo;

             $ava_get=$avatarload->getAvatar();
            if($ava_get=='')
             {
                 $ava_get='/images/no_foto.png';

            }

            return $ava_get;

}
    
         
         
       

            public function actionIndex(){
                $userinfo_arr=[];
           $user_id=Yii::$app->user->getid();
                    //загрузка googlemap//  
                   $user_coord=new Map_coords;
                   $mapcoord=$user_coord->select_map_coord($user_id);

                    if(count($mapcoord)== 0){ $mapcoord=' ';};


                     //вывод интересов//
                    $interests= new Interests;
                    $interes=$interests->get_interest($user_id);

                      $nteres_arr=$interes;
                    $json_iners_arr=json_encode ($nteres_arr);
                    
                    
                    //вывод значений полей инпут//
                    $userinfo=new Userinfo;
                    $userinfo_rez= $userinfo->getuserinfo($user_id);
                                                      
                   if(count($userinfo_rez) == 0){
                       
  
                       //создаем запись с id ползователя//
                       $userinfo->setuser_id_in_info();
                       
                       //зпаихиваем пустые занчения в массив//
                        $userinfo_arr[]= array('name'=>'','surname'=>'','job_place'=>'','age'=>'','website'=>'');
            
                    }else{
                      $userinfo_arr=$userinfo_rez;
                   
                    }

     
                   // вывод инфы в шаблон//

                    return $this->render('personal_settings',['json_iners_arr'=>$json_iners_arr,'mapcoord'=>$mapcoord,'userinfo_arr'=>$userinfo_arr]);

             }
            
                public function actionDelinteres(){
                    $interes_id=$_POST['interes_id'];
                    $interests= new Interests;
                    foreach($interes_id as $key){
                        $interests->del_interest($key);
                    };
                }

        public function actionSetuser_info(){
            
            //временная зона//
            
        
            //проверка на пост запрос
          
            if(Yii::$app->request->post()){
                
                $user_coord=new Map_coords;
                $user_id=Yii::$app->user->getid();
            
                 ///стависм  тайм зону//
          if(Yii::$app->request->post('user_time_zone')){
             $time_zone=Yii::$app->request->post('user_time_zone');
             $user_coord->set_time_zone( $user_id, $time_zone); 
           }	

                //запрос на googlemap//
                    if(Yii::$app->request->post('lat') && Yii::$app->request->post('lng')){  

                        $lng= Yii::$app->request->post('lng');   
                        $lat= Yii::$app->request->post('lat');  
                        $location=Yii::$app->request->post('location');  

                        if($user_coord->find_map_coord($user_id)>0){
                           $user_coord->update_map_coord($user_id,$lat,$lng, $location);

                        }else{

                            $user_coord->insert_map_coord($user_id,$lat,$lng, $location);

                        }
                    }
                    
                        //запрос на Интересы//
                     if(Yii::$app->request->post('interes')){
          
                        $user_interes_obj=new Interests;

                         $interes= Yii::$app->request->post('interes');  
                        $user_id=Yii::$app->user->getid();

                        $count=$user_interes_obj->count_interests($user_id);

                         foreach($interes as $key){ 
                            $user_interes_obj->set_interests($user_id,$key);

                        };
                      
                        //вывод обновленной инфформации//
            
                        $interes=$user_interes_obj->get_interest(Yii::$app->user->getid());
                        return(json_encode($interes));
                    }
                
                    
                                    //запрос на ФИО или Саит или Работа и т.д//
                 if(Yii::$app->request->post('userinput')){
                            $userinfo=new Userinfo;
                

                    $rez=Yii::$app->request->post('userinput');
                    

                 foreach($rez as $key => $value){

                            if($value['cur_input_name']==='user-surname')

                              {$user_surname=$value['cur_input_value'];
                                                           
                                $userinfo->setsurname($user_surname);
                              }
                              
                                    if($value['cur_input_name']==='user-name')

                              {$user_name=$value['cur_input_value'];
                                                           
                                $userinfo->updatename($user_name);
                              }

                            if($value['cur_input_name']==='user-job')

                              {$user_job=$value['cur_input_value'];
                                 $userinfo->setjob($user_job);
                              }


                            if($value['cur_input_name']==='user-age')

                              {$user_age=$value['cur_input_value'];
                               $userinfo->setage($user_age);
                              }


                            if($value['cur_input_name']==='user-website')

                              {$user_web=$value['cur_input_value'];
                                $userinfo->setwebsite($user_web);
                              }  
                              
                              
                            if($value['cur_input_name']==='user-email')

                              {$user_email=$value['cur_input_value'];
                                $userinfo->setemail($user_email);
                              }  
                              

                     }
                
          
                }
                
         }

     }
     
     public function actionGeneralSettings(){
         $proj = new Project;
           $general_settings=new General_settings;
           
         
           $userid_count=$general_settings->getuserid($this->getuser_id());
                  /*стаим ид пользователя в таблицу если его */
            
         
                if($userid_count == 0){
                    $general_settings->set_user_id($this->getuser_id());
                 
           
      }
           
         /*проходим массив выбранных полей*/
         $check_arr=Yii::$app->request->post("check_arr");

     
 if(array_key_exists('check',$check_arr)){
        foreach($check_arr['check'] as $key){
            
                switch($key){
                    case 'age':$general_settings->not_show_age($this->getuser_id(),'1');break;
                    case 'email':$general_settings->not_show_email($this->getuser_id(),'1');break;
                    case 'job_place':$general_settings->not_show_job($this->getuser_id(),'1');break;
                    case 'aboutme':$general_settings->not_show_interes($this->getuser_id(),'1');break;
                    case 'website':$general_settings->not_show_web($this->getuser_id(),'1');break;
                }

        }
  }    
  
    if(array_key_exists('nocheck',$check_arr)){
                foreach($check_arr['nocheck'] as $key){
            
                switch($key){
                    case 'age':$general_settings->not_show_age($this->getuser_id(),'0');break;
                    case 'email':$general_settings->not_show_email($this->getuser_id(),'0');break;
                    case 'job_place':$general_settings->not_show_job($this->getuser_id(),'0');break;
                    case 'aboutme':$general_settings->not_show_interes($this->getuser_id(),'0');break;
                    case 'website':$general_settings->not_show_web($this->getuser_id(),'0');break;
                }

        }
         
    }    
    
        if(array_key_exists('radio_val',$check_arr)){
            
           
          
                
                if($check_arr['radio_val'] =='del'){$proj->del_proj_files($this->getuser_id());$proj->del_proj($this->getuser_id());}
                   if($check_arr['radio_val'] =='hide'){$proj->hide_proj($this->getuser_id());}
            

    }   
    
    

        
        
     }
     
                public function actionShowCheck(){
                    
                    $user_id=Yii::$app->user->getid();
                 $userinfo_settings= new General_settings;
                  $userinfo_set_rez=$userinfo_settings->get_all_info($user_id);
                  $check_arr=[];
                  
            
              if(is_array($userinfo_set_rez)){
                      
                        foreach($userinfo_set_rez as $key=>$value){
                           if ($value == 1){
               
                                    /*убираем название  usinfo_ чтобы ключ сопадал с название поля из таблицы userinfo*/
                                    $key=substr($key,7);

                                  $check_arr['check_arr'][$key]= $key;

                            }
                        }

                  }else{$check_arr['nocheck'] = 'nocheck';}
       return json_encode($check_arr);
                }
}

